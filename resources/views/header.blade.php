<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="navbar" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="{{route('user_reminders')}}">Get Reminder <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{route('add_reminder_view')}}">Add Reminder</a>
      </li>
    </ul>
  </div>
</nav>
