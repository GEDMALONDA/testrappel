@extends('Main')
@section('content')
    <div class="container">
        <br>
        <div class="row justify-content-center">
            <div class="col-8" id="getreminder">
                <h4 class="text-center">Get Reminder</h4>
                <table id="reminders">
                    <tr>
                        <th>Name</th>
                        <th>Hour</th>
                        <th>Date</th>
                        <th>CRUD</th>
                    </tr>
                    @foreach($reminders as $reminder)
                    <tr>
                        <td> {{$reminder->name}} </td>
                        <td>{{$reminder->hour}}</td>
                        <td>{{$reminder->date}}</td>
                        <td class="d-flex justify-content-center">
                            <a href="{{route('show_reminder', $reminder->id) }}" class="btn btn-success p-2">See</a>
                            <a href="{{route('update_reminder_view', $reminder->id) }}" class="btn btn-warning p-2">Update</a>
                            <a href="{{route('delete_reminder', $reminder->id) }}" class=" btn btn-danger p-2">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
