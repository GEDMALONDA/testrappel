@extends('Main')
@section('content')
    <div class="container">
        <br>
        <div class="row justify-content-center">
            <div class="col-6" id="addReminder">
                <h4 class="text-center">Show Reminder</h4>
                <form>
                    <label for="name">Name</label>
                    <input type="text" id="name" name="name" value="{{$reminder->name}}" readonly>

                    <label for="hour">Hour</label>
                    <input type="text" id="hour" name="hour" value="{{$reminder->hour}}" readonly>

                    <label for="hour">Date</label>
                    <input type="text" id="date" name="date" value="{{$reminder->date}}" readonly>
                </form>
            </div>
        </div>
    </div>
@endsection
