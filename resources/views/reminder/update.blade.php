@extends('Main')
@section('content')
    <div class="container">
        <br>
        <div class="row justify-content-center">
            <div class="col-6" id="addReminder">
                <h4 class="text-center">Update Reminder</h4>
                <form method="POST" action="{{route('update_reminder')}}">
                    @csrf
                    <input type="text" id="id" name="id" value="{{$reminder->id}}" hidden>

                    <label for="name">Name</label>
                    <input type="text" id="name" name="name" value="{{$reminder->name}}">
                    @error('name')
                    <div class="alert alert-danger" role="alert">{{ $message }}</div>
                    @enderror
                    <label for="hour">Hour</label>
                    <input type="text" id="hour" name="hour" value="{{$reminder->hour}}">
                    @error('hour')
                    <div class="alert alert-danger" role="alert">{{ $message }}</div>
                    @enderror
                    <label for="date">Date</label>
                    <input type="text" id="date" name="date" value="{{$reminder->date}}">
                    @error('date')
                    <div class="alert alert-danger" role="alert">{{ $message }}</div>
                    @enderror
                    <input type="submit" value="Submit">
                </form>
            </div>
        </div>
    </div>
@endsection
