<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ReminderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Route::name('user_reminders')->get('/user_reminders','ReminderController@index');
Route::name('add_reminder_view')->get('/add_reminder_view', 'ReminderController@addReminderView');
Route::name('show_reminder')->get('/show_reminder/{id}', 'ReminderController@showReminder');
Route::name('update_reminder_view')->get('/update_reminder_view/{id}', 'ReminderController@updateReminderView');

Route::group(['prefix' =>'reminder'], function($group){
    Route::name('update_reminder')->post('update', 'ReminderController@updateReminder');
    Route::name('add_reminder')->post('add', 'ReminderController@addReminder');
    Route::name('delete_reminder')->get('delete/{id}', 'ReminderController@deleteReminder');
});
Auth::routes();


Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

