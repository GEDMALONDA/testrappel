<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Reminder;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ReminderRequest;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;

class ReminderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view("reminder.index", $this->getReminders());
    }

    public function getReminders()
    {
        $reminders = Cache::remember('test', 30,function(){
         return Reminder::where('user_id', Auth::id())->get();
        });  
        return (compact('reminders'));
    }

    public function addReminderView()
    {
        return view("reminder.add");
    }

    public function addReminder(ReminderRequest $request)
    {
        Reminder::create([
            'name' => $request->get('name'),
            'hour' => $request->get('hour'),
            'date' => $request->get('date'),
            'user_id' => Auth::id(),
        ]);
        Artisan::call('cache:clear');
        return view('reminder.index', $this->getReminders())->with('successNotif','reminder successfully added');
    }

    public function showReminder(int $id)
    {
        $reminder = Reminder::where('id', $id)->first();
        return view('reminder.show', compact('reminder'));
    }

    public function deleteReminder(int $id)
    {
        Reminder::where('id', $id)->first()->delete();
        Artisan::call('cache:clear');
        return view('reminder.index', $this->getReminders())->with('successNotif','reminder successfully deleted');
    }

    public function updateReminderView(int $id)
    {
        $reminder = Reminder::where('id', $id)->first();
        return view('reminder.update', compact('reminder'));
    }

    public function updateReminder(ReminderRequest $request)
    {
        if ($request->get('id')!= null) {
            $reminder = Reminder::where('id', $request->get('id'))->first();
            if ($reminder['user_id']== Auth::id()) {
                $reminder->update([
                    'name' => $request->get('name'),
                    'hour' => $request->get('hour'),
                    'date' => $request->get('date'),
                ]);
            }
            Artisan::call('cache:clear');
            return view('reminder.index', $this->getReminders())->with('successNotif','reminder successfully updated');
        }
    }
}
