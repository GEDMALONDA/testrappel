<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Reminder extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'hour',
        'date',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function like()
    {
        $like = new Like(['user_id' => Auth::id()]);
        $this->likes()->save($like);
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }
    public function isliked()
    {
        return !!$this->likes()->where('user_id', Auth::id())->count();
    }
}
