<?php

namespace Tests\Feature;

use App\Models\Reminder;
use App\Http\Controllers\ReminderController;
use App\Models\User;
use Faker\Factory;
// use Illuminate\Foundation\Testing\RefreshDatabase;
// use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

use function PHPUnit\Framework\assertEquals;

class ReminderTest extends TestCase
{
    public function test_get_users(): void
    {
        $users = User::all();
        $this->assertCount(1, $users);
    }

    public function test_only_logged_user_can_see_reminders(): void
    {
        $this->get('/user_reminders')->assertRedirect('/login');
    }

    public function test_get_first_user_reminders(): void
    {
        $user = User::all()->first();
        $this->assertCount(2, $user->reminders);
    }

    public function test_user_has_a_name(): void
    {
        $user = new User([
            'name' => 'JAELLE',
            'email' => 'jaelle@jaelle.fr'
        ]);
        $this->assertEquals('JAELLE', $user->name);
    }

    public function test_user_can_add_reminder(): void
    {
        $user = User::all()->first();
        $user->reminders->push(new Reminder());
        $this->assertCount(3, $user->reminders);
    }

    public function test_user_can_like_reminder(): void
    {
        $user = User::factory()->create();
        $reminder = Reminder::factory()->create();

        $this->actingAs($user);
        $reminder->like();

        $this->assertDatabaseHas('likes', [
            'user_id' => $user->id,
            'likeable_id' => $reminder->id,
            'likeable_type' => get_class($reminder),
        ]);

        $this->assertTrue($reminder->isLiked());
    }
}
